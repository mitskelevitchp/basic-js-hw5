// Теоретичні питання
/*1. Метод об'єкту - це властивість об'єкту, значення якої є функцією. За допомогою цієї функції (цього методу) можна здійснювати різноманітні операції над іншими значеннями та властивостями значень цього об'єкту.
Наприклад, така функція може фільтрувати введені користувачем дані, що присвоюються як значення іншим  властивостям: при введенні одних даних змінювати значення властивостей, других - ні, над третіми виконувати певні операції (повернення середнього значення тощо).
2. Будь-які значення: бути числами, строками, масивами та ін.
3. Наприклад, змінна а має значення "30". Ми можемо ініціювати змінну b та надати їй значення через b = a. Якщо змінити значення a, значення змінної b після цього не зміниться:
let a = 30;
let b = a;
a = 40;
console.log(a); // 40
console.log(b); // 30
a та b тут незалежні одна від одної елементи.
Об'єкти працюють інакше. Вираз "b = a" не зможе скопіювати значення об'єкту а та надати його значенню об'єкта b. Якщо змінна є об'єктом, вона не є незалежним елементом, але зберігає в собі посилання на об'єкт. Тому при "b = a" значенням змінної b буде посилання на той самий об'єкт, що "ховається" в змінній a.*/

// Практичне завдання
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

// const newUser = {};
// function createNewUser() {
//   newUser.firstName = prompt("Вкажіть, будь ласка, ваше ім'я:");
//   newUser.lastName = prompt("Вкажіть, будь ласка, ваше прізвище:");
//   newUser.login =
//     newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase();
//   newUser.getLogin = function () {
//     this.login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
//     return this.login;
//   };
//   return newUser;
// }
// createNewUser();
// newUser.getLogin();

// Додаткове завдання
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
const newUser = {};
function createNewUser() {
  Object.defineProperty(newUser, "firstName", {
    set: function setFirstName(value) {
      if (value.length < 3) {
        alert("NO!");
      } else {
        return (this._firstName = value);
      }
    },
  });
  Object.defineProperty(newUser, "lastName", {
    set: function setLastName(value) {
      if (value.length < 3) {
        alert("NO!");
      } else {
        return (this._lastName = value);
      }
    },
  });

  newUser.firstName = prompt("Вкажіть ім'я (к-сть літер більше 3):");
  newUser.lastName = prompt("Вкажіть прізвище (к-сть літер більше 3):");
  newUser.login =
    newUser._firstName[0].toLowerCase() + newUser._lastName.toLowerCase();

  newUser.getLogin = function () {
    newUser.login =
      newUser._firstName[0].toLowerCase() + newUser._lastName.toLowerCase();
    return newUser.login;
  };

  return newUser;
}

createNewUser();
console.log(newUser);
newUser.getLogin();
